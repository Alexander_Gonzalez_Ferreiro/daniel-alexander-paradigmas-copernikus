package copernikus


@main def telescopioServiceMain = {
  object TelescopioService extends TelescopioService
  import TelescopioService.*

  val parasol = Parasol("SinDesplegar")

  val telescopio = Telescopio(
    parasol: Parasol,
    0
  )

  val telescopioResultado = TelescopioService.abrirParasol(telescopio, 50)
  println(telescopioResultado.parasol.status)

}

