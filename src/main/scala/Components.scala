package copernikus

enum estadoParasol:
  case SinDesplegar, Desplegado

case class Telescopio(
 parasol: Parasol,
 temperatura: Int
)

// companion object
/*object Telescopio {
  def apply(parasol: Option[Parasol],
            temperatura: Int,
            segmentos: Seq[Segmento]): Telescopio = {
    var telescopio = new Telescopio (parasol, temperatura , null)
  }

}*/

case class Parasol(
  status: String
)

case class Espejo(
  segmentos: Seq[Segmento]
)

case class Segmento(
  numero: Int,
  status: String,
  gradosInclinacion: Int
)

case class Sensor(
  foto: String
)